package thomasave.notes;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.Calendar;

public class SetAlarm extends Activity {

    private static final int RQS_1 = 1337;
    SharedPreferences sharedpreferences;
    Calendar calNow = Calendar.getInstance();
    public static final String MyPREFERENCES = "MyPrefs" ;
    public String note;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getBaseContext().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Integer selectedHour = sharedpreferences.getInt("SelectedHour",0);
        Integer selectedMinute = sharedpreferences.getInt("SelectedMinute",0);
        Integer selectedYear = sharedpreferences.getInt("SelectedYear", 0);
        Integer selectedMonth = sharedpreferences.getInt("SelectedMonth", 0);
        Integer selectedDay = sharedpreferences.getInt("SelectedDay", 0);
        note = sharedpreferences.getString("note", "Koopeenecologischeheusnoornvoormaar2euro1253");
        if (!note.equals("Koopeenecologischeheusnoornvoormaar2euro1253")){
            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();

            calSet.set(Calendar.HOUR_OF_DAY, selectedHour);
            calSet.set(Calendar.MINUTE, selectedMinute);
            calSet.set(Calendar.YEAR, selectedYear);
            calSet.set(Calendar.MONTH, selectedMonth);
            calSet.set(Calendar.DAY_OF_MONTH, selectedDay);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if(calSet.compareTo(calNow) <= 0){
                //Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }
            setAlarm(calSet);

        } else {
            finish();
        }
     }

    private void setAlarm(Calendar targetCal){
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);
        notification(targetCal);
    }

    private void notification(Calendar c) {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentTitle(note);
        String date = "";
        String curTime = String.format("%02d:%02d", c.getTime().getHours(), c.getTime().getMinutes());
        if (c.getTime().getDate() == calNow.getTime().getDate() && c.getTime().getMonth() == calNow.getTime().getMonth() && c.getTime().getYear() == calNow.getTime().getYear()) {
            date = "";
        } else {
            String datenow = String.format("%02d/%02d/%04d",c.getTime().getDate(), c.getTime().getMonth()+1, c.getTime().getYear()+1900);
            date = " on " + datenow;
        }
        builder.setContentText("Alarm is set for " + curTime + date);
        builder.setTicker("Alarm is set for " + curTime + date);
        builder.setOngoing(true);
        builder.setSmallIcon(R.drawable.ic_action_alarms);
        Intent intentzetaf = new Intent(this, MainActivity.class);
        intentzetaf.putExtra("alarm","moetstoppen");
        PendingIntent pintent = PendingIntent.getActivity(this, 0, intentzetaf, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_action_cancel, "Stop alarm",pintent);
        Notification notifications = builder.build();
        NotificationManager notificationManger =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManger.notify(01, notifications);

    }
}
