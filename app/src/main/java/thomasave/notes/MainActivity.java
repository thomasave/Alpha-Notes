package thomasave.notes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends Activity {

    private static final String TAG = "";
    ArrayList<String> inputitems = new ArrayList<String>();
    ArrayList<String> inputitem = new ArrayList<String>();
    Boolean isdiscarded = false;
    Boolean isalarmon = false;
    Calendar calNow = Calendar.getInstance();
    final Calendar calSet = (Calendar) calNow.clone();
    ListView output;
    String status = "Full";
    EditText edittext;
    public static final String MyPREFERENCES = "MyPrefs" ;
    SharedPreferences sharedpreferences;
    int a;
    String note = "Koopeenecologischeheusnoornvoormaar2euro1253";
    private static final int TIME_DIALOG_ID = 9999;
    private static final int DATE_DIALOG_ID = 2222;
    private static final int RQS_1 = 1337;
    Calendar c = Calendar.getInstance();
    Context context;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Obtain the shared Tracker instance.
        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        if (savedInstanceState != null) {
            inputitems = savedInstanceState.getStringArrayList("inputitems");
        }
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        if (sharedpreferences.getString("adpreference","Full").equals("Full") ) {
            AdView mAdView = (AdView) this.findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
            status = "Full";
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Ads")
                    .setAction("Full")
                    .build());
        } else {
            Log.v("adpreference", "destroy");
            View adfragment = findViewById(R.id.ad_fragment);
            adfragment.setVisibility(View.INVISIBLE);
            status = sharedpreferences.getString("adpreference","None");
            mTracker.send(new HitBuilders.EventBuilder()
                    .setCategory("Ads")
                    .setAction(sharedpreferences.getString("adpreference","None"))
                    .build());
        }
        inputitems.clear();
        int size = sharedpreferences.getInt("Status_size", 0);
        note = sharedpreferences.getString("note", "Koopeenecologischeheusnoornvoormaar2euro1253");
        Log.v("STRING EXTRA", getIntent().getStringExtra("alarm")+"");
        for (int i = 0; i < size; i++) {
            inputitems.add(sharedpreferences.getString("Status_" + i, null));
        }

        try {
            if (getIntent().getStringExtra("alarm").equals("gaat af") && (!isalarmon)) {
                Log.v("CREATE","ALARM");
                alarmon();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            if (getIntent().getStringExtra("alarm").equals("moetstoppen")) {
                new AlertDialog.Builder(this)
                        .setTitle("Stop Alarm")
                        .setMessage(Html.fromHtml("Do you want to stop the alarm for "+ "<b>" + note +"</b>"+" ?"))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.v("CREATE", "STOPT");
                                NotificationManager notificationManger =
                                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManger.cancel(01);
                                Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(pendingIntent);
                                note = "Koopeenecologischeheusnoornvoormaar2euro1253";
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                mEdit1.putString("note", note);
                                mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                                for (int i = 0; i < inputitems.size(); i++) {
                                    mEdit1.remove("Status_" + i);
                                    mEdit1.putString("Status_" + i, inputitems.get(i));
                                }
                                mEdit1.commit();
                                Intent intents = new Intent(context, MainActivity.class);
                                finish();
                                startActivity(intents);
                                edittext.setTextIsSelectable(true);


                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
            edittext = (EditText) findViewById(R.id.edittext);
            edittext.setVisibility(View.INVISIBLE);
            setlist();
            context = this;
            listener();
            edittext.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

                public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                    try {
                        edittext.setVisibility(View.VISIBLE);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    try {
                        output.setVisibility(View.INVISIBLE);
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    return true;
                }

                public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                    return false;
                }

                public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                    return false;
                }

                public void onDestroyActionMode(ActionMode actionMode) {
                    if (!isdiscarded) {
                        inputitems.set(a,edittext.getText().toString());
                    }
                    inputitem.clear();
                    setlist();
                    listener();
                }
            });

    }


    private void listener() {
        output.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                inputitem.add(inputitems.get(i));
                isdiscarded = false;
                a = i;
                output.setOnItemLongClickListener(null);
                startActionMode(mActionModeCallback);
                return true;
            }
        });
    }

    private void setlist() {
        try {
             edittext.setVisibility(View.INVISIBLE);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            output.setVisibility(View.VISIBLE);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        output = (ListView) findViewById(R.id.listView);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.items, inputitems);
        output.setAdapter(adapter);
    }

    private void setlist2() {
        try {
            edittext.setVisibility(View.VISIBLE);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            output.setVisibility(View.INVISIBLE);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        edittext.setText(inputitem.get(0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_deleteall) {
            if (note.equals("Koopeenecologischeheusnoornvoormaar2euro1253")) {
                Log.v(note,inputitems.get(a));
                new AlertDialog.Builder(context)
                        .setTitle("Delete All")
                        .setMessage("Are you sure you want to delete all items?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                inputitems.clear();
                                setlist();
                                isdiscarded = true;
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                                for(int i=0;i<inputitems.size();i++)
                                {
                                    mEdit1.remove("Status_" + i);
                                    mEdit1.putString("Status_" + i, inputitems.get(i));
                                }
                                mEdit1.commit();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            } else {

                Log.v("== "+note,inputitems.get(a));
                new AlertDialog.Builder(context)
                        .setTitle("Delete")
                        .setMessage(Html.fromHtml("There has an alarm been set for <b>"+note+"</b> , do you wish to continue?"))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                inputitems.clear();
                                setlist();
                                isdiscarded = true;
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */
                                for(int i=0;i<inputitems.size();i++)
                                {
                                    mEdit1.remove("Status_" + i);
                                    mEdit1.putString("Status_" + i, inputitems.get(i));
                                }
                                mEdit1.commit();

                                new AlertDialog.Builder(context)
                                        .setTitle("Delete")
                                        .setMessage("Do you also wish to disable the alarm?" )
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                NotificationManager notificationManger =
                                                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                notificationManger.cancel(01);
                                                Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                                AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                                                alarmManager.cancel(pendingIntent);
                                                note="Koopeenecologischeheusnoornvoormaar2euro1253";
                                            }
                                        })
                                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // do nothing
                                            }
                                        })
                                        .show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();

            }
    }

        if (id == R.id.action_add) {
            add();
            return true;
        }
        if (id == R.id.action_settings) {
            Intent intentsettings = new Intent(this, settings.class);
            startActivity(intentsettings);
            return true;
        }
        if (id == R.id.action_about) {
            Intent intentabout = new Intent(this, about.class);
            startActivity(intentabout);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void add() {
        inputitems.add(0, "");
        a = 0;
        inputitem.add("");
        isdiscarded = false;
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("New Note")
                .build());
        startActionMode(mActionModeCallback);
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            // assumes that you have "contexual.xml" menu resources
            inflater.inflate(R.menu.actionmodeitems, menu);
            setlist2();
            if (sharedpreferences.getString("adpreference","Full").equals("Less") ) {
                View adfragment = findViewById(R.id.ad_fragment);
                adfragment.setVisibility(View.VISIBLE);
                AdView mAdView = (AdView) findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder().build();
                mAdView.loadAd(adRequest);
            }
            return true;
        }

        // called each time the action mode is shown. Always called after
        // onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false; // Return false if nothing is done
        }

        // called when the user selects a contextual menu item
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_discard:
                    if (note.equals(inputitems.get(a))) {
                        Log.v("== "+note,inputitems.get(a));
                        new AlertDialog.Builder(context)
                                .setTitle("Delete")
                                .setMessage("There has an alarm been set for this note, do you wish to continue?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        inputitems.remove(a);
                                        setlist();
                                        isdiscarded = true;
                                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */
                                        for(int i=0;i<inputitems.size();i++)
                                        {
                                            mEdit1.remove("Status_" + i);
                                            mEdit1.putString("Status_" + i, inputitems.get(i));
                                        }
                                        mEdit1.commit();

                                        new AlertDialog.Builder(context)
                                                .setTitle("Delete")
                                                .setMessage("Do you also wish to disable the alarm?" )
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        NotificationManager notificationManger =
                                                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                        notificationManger.cancel(01);
                                                        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                                        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                                                        alarmManager.cancel(pendingIntent);
                                                        note="Koopeenecologischeheusnoornvoormaar2euro1253";
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // do nothing
                                                    }
                                                })
                                                .show();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    } else {
                        Log.v(note,inputitems.get(a));
                        inputitems.remove(a);
                        setlist();
                        isdiscarded = true;
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                        for(int i=0;i<inputitems.size();i++)
                        {
                            mEdit1.remove("Status_" + i);
                            mEdit1.putString("Status_" + i, inputitems.get(i));
                        }
                        mEdit1.commit();
                    }

                    mode.finish(); // Action picked, so close the CAB
                    return true;
                case R.id.action_about:
                    Intent intentabout = new Intent(context, about.class);
                    startActivity(intentabout);
                    mode.finish();
                    return true;
                case R.id.action_alarm:
                    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.JELLY_BEAN) {
                        if (note.equals("Koopeenecologischeheusnoornvoormaar2euro1253")) {
                            Log.v("Note = 0", note);
                            showDialog(TIME_DIALOG_ID);
                            note = inputitems.get(a);
                            sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                            mEdit1.putString("note", note);
                            mEdit1.commit();
                        } else {
                            Log.v("Note =/= 0", note + " - End note");
                            new AlertDialog.Builder(context)
                                    .setTitle("Set Alarm")
                                    .setMessage("There is already an alarm set for " + note + ". This alarm will be overwritten, do you wish to continue?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            NotificationManager notificationManger =
                                                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                            notificationManger.cancel(01);
                                            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                            alarmManager.cancel(pendingIntent);
                                            showDialog(TIME_DIALOG_ID);
                                            note = inputitems.get(a);
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // do nothing
                                        }
                                    })
                                    .show();
                        }
                        mode.finish();
                        return true;
                    } else {
                        new AlertDialog.Builder(context)
                                .setTitle("Sorry")
                                .setMessage("This function is only available for Android Jellybean or higher.")
                                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                        mode.finish();
                        return true;
                    }
                case R.id.action_deleteall:
                    if (note.equals("Koopeenecologischeheusnoornvoormaar2euro1253")) {
                        Log.v(note,inputitems.get(a));
                        new AlertDialog.Builder(context)
                                .setTitle("Delete All")
                                .setMessage("Are you sure you want to delete all items?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        inputitems.clear();
                                        setlist();
                                        isdiscarded = true;
                                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                                        for(int i=0;i<inputitems.size();i++)
                                        {
                                            mEdit1.remove("Status_" + i);
                                            mEdit1.putString("Status_" + i, inputitems.get(i));
                                        }
                                        mEdit1.commit();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                    } else {

                        Log.v("== "+note,inputitems.get(a));
                        new AlertDialog.Builder(context)
                                .setTitle("Delete")
                                .setMessage(Html.fromHtml("There has an alarm been set for <b>"+note+"</b> , do you wish to continue?"))
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        inputitems.clear();
                                        setlist();
                                        isdiscarded = true;
                                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */
                                        for(int i=0;i<inputitems.size();i++)
                                        {
                                            mEdit1.remove("Status_" + i);
                                            mEdit1.putString("Status_" + i, inputitems.get(i));
                                        }
                                        mEdit1.commit();
                                        new AlertDialog.Builder(context)
                                                .setTitle("Delete")
                                                .setMessage("Do you also wish to disable the alarm?" )
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        NotificationManager notificationManger =
                                                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                                        notificationManger.cancel(01);
                                                        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                                        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                                        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                                                        alarmManager.cancel(pendingIntent);
                                                        note="Koopeenecologischeheusnoornvoormaar2euro1253";
                                                    }
                                                })
                                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        // do nothing
                                                    }
                                                })
                                                .show();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();

                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        // called when the user exits the action mode
        public void onDestroyActionMode(ActionMode mode) {
            if (sharedpreferences.getString("adpreference","Full").equals("Less") ) {
                View adfragment = findViewById(R.id.ad_fragment);
                adfragment.setVisibility(View.INVISIBLE);
            }
            if (!isdiscarded) {
                inputitems.set(a,edittext.getText().toString());
            }
            hideSoftKeyboard();
            inputitem.clear();
            setlist();
            listener();
        }
    };
    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this,
                        timePickerListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE),true);
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this, datePickerListener,c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
        calSet.set(Calendar.YEAR, i);
        calSet.set(Calendar.MONTH, i2);
        calSet.set(Calendar.DAY_OF_MONTH, i3);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
        mEdit1.putInt("SelectedYear", i);
        mEdit1.putInt("SelectedMonth", i2);
        mEdit1.putInt("SelectedDay", i3);
        mEdit1.commit();
        setAlarm(calSet);
        }
    };

    private TimePickerDialog.OnTimeSetListener timePickerListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int selectedHour,
                                      int selectedMinute) {

                    calSet.set(Calendar.HOUR_OF_DAY, selectedHour);
                    calSet.set(Calendar.MINUTE, selectedMinute);
                    calSet.set(Calendar.SECOND, 0);
                    calSet.set(Calendar.MILLISECOND, 0);

                    sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                    mEdit1.putInt("SelectedHour", selectedHour);
                    mEdit1.putInt("SelectedMinute", selectedMinute);
                    mEdit1.commit();
                    showDialog(DATE_DIALOG_ID);
                }};

    private void setAlarm(Calendar targetCal){
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);
        notification(targetCal);
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("SetAlarm")
                .build());
    }

    private void notification(Calendar c) {
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentTitle(note);
        String date = "";
        String curTime = String.format("%02d:%02d", c.getTime().getHours(), c.getTime().getMinutes());
        if (c.getTime().getDate() == calNow.getTime().getDate() && c.getTime().getMonth() == calNow.getTime().getMonth() && c.getTime().getYear() == calNow.getTime().getYear()) {
            date = "";
        } else {
            String datenow = String.format("%02d/%02d/%04d",c.getTime().getDate(), c.getTime().getMonth()+1, c.getTime().getYear()+1900);
            date = " on " + datenow;
        }
        builder.setContentText("Alarm is set for " + curTime + date);
        builder.setTicker("Alarm is set for " + curTime + date);
        builder.setOngoing(true);
        builder.setSmallIcon(R.drawable.ic_action_alarms);
        Intent intentzetaf = new Intent(context, MainActivity.class);
        intentzetaf.putExtra("alarm","moetstoppen");
        PendingIntent pintent = PendingIntent.getActivity(context, 0, intentzetaf, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.addAction(R.drawable.ic_action_cancel, "Stop alarm",pintent);
        builder.setContentIntent(pintent);
        Notification notifications = builder.build();
        NotificationManager notificationManger =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManger.notify(01, notifications);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.v("OnNewIntent", "Active");
        Log.v("STRING ONNEWINTENT", intent.getStringExtra("alarm")+"");
        try {
            if (intent.getStringExtra("alarm").equals("gaat af")) {
                Log.v("ONNEWINTENT","ALARM");
                alarmon();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
        try {
            if (intent.getStringExtra("alarm").equals("moetstoppen")) {
                new AlertDialog.Builder(this)
                        .setTitle("Stop Alarm")
                        .setMessage(Html.fromHtml("Do you want to stop the alarm for "+ "<b>" + note +"</b>"+" ?"))
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Log.v("CREATE","STOPT");
                                NotificationManager notificationManger =
                                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManger.cancel(01);
                                Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(), RQS_1, intent, 0);
                                AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
                                alarmManager.cancel(pendingIntent);
                                note="Koopeenecologischeheusnoornvoormaar2euro1253";
                                sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                                mEdit1.putString("note", note);
                                mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                                for(int i=0;i<inputitems.size();i++)
                                {
                                    mEdit1.remove("Status_" + i);
                                    mEdit1.putString("Status_" + i, inputitems.get(i));
                                }
                                mEdit1.commit();
                                Intent intents = new Intent(context, MainActivity.class);
                                finish();
                                startActivity(intents);

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void alarmon() {
        Log.v("Alarmon","IS REACHED");
        isalarmon = true;
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setContentTitle(note);
        builder.setContentText("Time for " + note + " has been reached");
        builder.setTicker(note);
        builder.setSmallIcon(R.drawable.ic_action_alarms);
        Notification notifications = builder.build();
        final NotificationManager notificationManger =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManger.cancel(01);
        notificationManger.notify(02, notifications);
        SharedPreferences getAlarms = PreferenceManager.
                getDefaultSharedPreferences(getBaseContext());
        String alarms = getAlarms.getString("Alarm Sound", RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString());
        Uri alarm = Uri.parse(alarms);
        final MediaPlayer mPlayer = MediaPlayer.create(getApplicationContext(), alarm);
        mPlayer.reset();
        mPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
        try {
            mPlayer.setDataSource(getApplicationContext(), alarm);
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            mPlayer.prepare();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        }
        mPlayer.setLooping(true);
        mPlayer.start();
        new AlertDialog.Builder(this)
                .setTitle("Alarm")
                .setMessage(note)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mPlayer.stop();
                        isalarmon = false;
                        note="Koopeenecologischeheusnoornvoormaar2euro1253";
                        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
                        mEdit1.putString("note", note);
                        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

                        for(int i=0;i<inputitems.size();i++)
                        {
                            mEdit1.remove("Status_" + i);
                            mEdit1.putString("Status_" + i, inputitems.get(i));
                        }
                        mEdit1.commit();
                        Intent intent = new Intent(context, MainActivity.class);
                        finish();
                        startActivity(intent);

                    }

                })
                .show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putStringArrayList("inputitems", inputitems);
        savedInstanceState.putString("note", note);

    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        inputitems = savedInstanceState.getStringArrayList("inputitems");
        note = savedInstanceState.getString("note");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("", "Setting screen name: " + "Mainactivity");
        mTracker.setScreenName("Mainactivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        View adfragment = findViewById(R.id.ad_fragment);
        String statusafter = sharedpreferences.getString("adpreference", "Full");
        if (statusafter.equals("Full") && !(status.equals("Full"))) {
            AdView mAdView = (AdView) this.findViewById(R.id.adView);
            AdRequest adRequest = new AdRequest.Builder().build();
            mAdView.loadAd(adRequest);
            adfragment.setVisibility(View.VISIBLE);
        }

        if (!(statusafter.equals("Full")) && status.equals("Full")) {
            adfragment.setVisibility(View.INVISIBLE);
            status = statusafter;

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor mEdit1 = sharedpreferences.edit();
        mEdit1.putInt("Status_size", inputitems.size()); /* sKey is an array */

        for(int i=0;i<inputitems.size();i++)
        {
            mEdit1.remove("Status_" + i);
            mEdit1.putString("Status_" + i, inputitems.get(i));
        }
        mEdit1.putString("note", note);
        mEdit1.commit();
    }
}